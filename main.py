#!/usr/bin/env python
# -*- coding: utf-8 -*-
from datetime import datetime

from flask import (
    Flask, jsonify, request
)


def configure_routes(app):
    @app.route('/', methods=['GET'])
    def index():
        return jsonify(
            now=datetime.utcnow(),
            headers=dict(request.headers)
        )


def app_factory(*args, **kwargs):
    print(*args, **kwargs)
    app = Flask(__name__)

    configure_routes(app)

    return app


app = app_factory()


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=3013)
