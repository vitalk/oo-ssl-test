FROM python:3.6

MAINTAINER Vital Kudzelka "vital.kudzelka@leverx.com"

RUN apt update -y && \
    apt install -y --no-install-recommends \
    libmysqlclient-dev && \
    rm -rf /var/lib/apt/lists/*

WORKDIR /src
COPY . /src

RUN pip install -r requirements.txt
RUN pip install -U pip --no-cache-dir

ENV PYTHONPATH=.

EXPOSE 8080

CMD ["gunicorn", "main:app", "--bind", "0.0.0.0:8080", "--reload", "--access-logfile", "-", "--error-logfile", "-"]
